"""
Bjoern Annighoefer Mar 2019
"""

from .error import EoqError,EoqParseError,EoqRetrieveError,EoqCreateError,EoqIdError,EoqValueError,EoqUpdateError,EoqContextError,EoqTransactionError,EoqActionError,EoqGeneralError

__version__ = "1.0.0"
